﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LvlManager : Photon.PunBehaviour, IPunObservable
{
    //--------------------------------------
    // Public Variables
    //--------------------------------------

    public GameObject AICharacter;

    public enum Platform { Pc, Oculus, Hololens, Vive, Cardboard};

    private Platform currentPlatform;

    static public LvlManager Instance;

    [Tooltip("The prefab array of the different kind of players that can be spawn according to the platform that this program is deployed to")]
    // The order of the array is [0] = PC, [1] = Oculus, [2] = Hololens, [3] = Vive, [4] = Mobile
    public GameObject[] playersPrefab;

    [Tooltip("Spawn Locations for the players")]
    public Transform[] spawnLocations;

    // FireBall that will explode
    public GameObject FireBall;

    // Initial Ball Position
    public Transform initialBallPosition;

    // Array of the players
    public PlayerInteraction[] playersInteractions;

    public GameObject room;

    // Script that sends reports to google form
    public DBConnection DBConnectionScript;

    //--------------------------------------
    // Private Variables
    //--------------------------------------

    // Total Time that will last the tingo, tingo, tango
    public int publicTotalTime;

    private int playerPosition;

    public bool isOnSession;

    private PlayerInteraction explodingPlayer;

    private int totalTime;

    //--------------------------------------
    // Methods
    //--------------------------------------
    // Use this for initialization
    void Start()
    {
        int platformSelected = PlayerPrefs.GetInt("Platform");
        currentPlatform = (Platform) platformSelected;
        Debug.Log("The platform selected is: " + currentPlatform.ToString());
        Instance = this;
        isOnSession = false;
        playerPosition = 0;
        if (playersPrefab == null)
        {
            //Debug.LogError("<Color=Red><a>Missing</a></Color> playerPrefab Reference. Please set it up in GameObject 'Game Manager'", this);
        }
        else
        {
            if(PhotonNetwork.isMasterClient)
            {
                spawnAIPlayers();
            }
            if (PlayerInteraction.LocalPlayerInstance == null)
            {
                PhotonPlayer actualPlayer = PhotonNetwork.player;
                Debug.Log("Nickname of the local is " + actualPlayer.NickName);
                spawnPlayer(actualPlayer.NickName, currentPlatform);
            }
            else
            {
                Debug.Log("Ignoring scene load for " + SceneManager.GetActiveScene().name);
            }
        }
        FireBall.GetComponent<PhotonView>().ownershipTransfer =  OwnershipOption.Request;
    }
    
    public void spawnAIPlayers()
    {
        int initialIndex = PhotonNetwork.room.PlayerCount;
        Debug.Log("There are " + initialIndex + " players online");
        for(int i = initialIndex; i < 4; i++)
        {
            Transform spawnPosition = spawnLocations[i];
            PhotonNetwork.Instantiate(AICharacter.name, spawnPosition.position, spawnPosition.rotation, 0);
        }
    }

    public void spawnPlayer(string playerNickname, Platform destPlatform)
    {
        char[] nickNamechars = playerNickname.ToCharArray();
        int playerId = Int32.Parse(nickNamechars[playerNickname.Length - 1] + "");
        Transform spawnPosition = spawnLocations[playerId];
        GameObject newPlayer = null;
        switch (destPlatform)
        {
            case Platform.Pc:
                newPlayer = playersPrefab[0];
                break;
            case Platform.Oculus:
                newPlayer = playersPrefab[1];
                break;
            case Platform.Hololens:
                newPlayer = playersPrefab[2];
                room.SetActive(false);
                break;
        }
        //Debug.Log("Player to be instantiated is: " + newPlayer.name);
        //Debug.Log("INFO IMPORTANTE : InstantiateOnNetwork is: " + PhotonNetwork.InstantiateInRoomOnly + " , inRoom: " + PhotonNetwork.inRoom);

        GameObject theNewPlayer = PhotonNetwork.Instantiate(newPlayer.name, spawnPosition.position, spawnPosition.rotation,0);
        theNewPlayer.GetComponent<PhotonView>().RPC("setPlayerInteraction", PhotonTargets.All, playerId);
    }


    // Method that initializes the game
    [PunRPC]
    public void initialize( int pTotalTime)
    {
        if (isOnSession == false)
        {
            totalTime = pTotalTime;
            //FireBallAudio.Instance.stopEffects();
            isOnSession = true;
            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
            foreach (GameObject temp in players)
            {
                PlayerInteraction playerI = temp.GetComponent<PlayerInteraction>();
                playerI.initialize(pTotalTime);
            }
            GameObject[] aiPlayers = GameObject.FindGameObjectsWithTag("AIPlayer");
            foreach (GameObject temp in aiPlayers)
            {
                PlayerInteraction playerI = temp.GetComponent<PlayerInteraction>();
                playerI.initialize(pTotalTime);
            }
            StartCoroutine(startCountDown());
            FireBallAudio.Instance.playEffect(0, true);
            FireBall.GetComponent<Animator>().SetBool("Boom", false);
            FireBall.transform.position = initialBallPosition.transform.position;
            initializeThrowing();
            Time.timeScale = 1f;
        }
    }



    // Method used to give the ball to some player
    public void initializeThrowing()
    {
        //int position = UnityEngine.Random.Range(0, PhotonNetwork.countOfPlayers);
        // Acá se sincronizaría el id o el número del jugador que recibiría la bola
        foreach (PlayerInteraction temp in playersInteractions)
        {
            if (temp.gameObject.GetComponent<PhotonView>().owner.IsMasterClient)
            {
                temp.GetComponentInChildren<PhotonView>().RPC("takeBall", PhotonTargets.All, -1);
                //temp.takeBall(-1);
                break;
            }
        }
    }

    /// <summary>
    /// Called when the local player left the room. We need to load the launcher scene.
    /// </summary>
    public override void OnLeftRoom()
    {
        //SceneManager.LoadScene(0);
    }

    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
    }


    // IEnumerator that begins the session timer
    IEnumerator startCountDown()
    {
        while (totalTime > 0)
        {
            totalTime -= 1;
            yield return new WaitForSeconds(1f);
           
        }
        photonView.RPC("explodeBall", PhotonTargets.All, null);
    }

    [PunRPC]
    public void explodeBall()
    {
        FireBall.GetComponent<Animator>().SetBool("Boom", true);
        FireBallAudio.Instance.stopEffects();
        FireBallAudio.Instance.playEffect(2, false);
        Time.timeScale = 0.8f;
        isOnSession = false;
        StopAllCoroutines();
        foreach (PlayerInteraction temp in playersInteractions)
        {
            if(temp.hasFireBall())
            {
                temp.explodeBall();
            }
        }
    }


    void Update()
    {
        if (Input.GetButtonDown("Submit"))
        {
            initializeGame();            
        }
    }

    public void initializeGame()
    {
        if ( PhotonNetwork.isMasterClient && !isOnSession)
        {
            //publicTotalTime = UnityEngine.Random.Range(30, 45);
            photonView.RPC("initialize", PhotonTargets.All, publicTotalTime);
        }
    }

    public Platform getCurrentPlatform()
    {
        return currentPlatform;
    }
    //--------------------------------------------------
    // Online
    //-------------------------------------------------

    public void sendReport(string time, string reactionTime)
    {
        string platform = currentPlatform.ToString();
#if UNITY_ANDROID
            platform = "Gear VR";
#endif
        DBConnectionScript.sendInfo(currentPlatform.ToString(), time, reactionTime);
        Debug.Log("Oh boy");
    }


    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            // We own this player. Send the other our data
           // stream.SendNext(totalTime);
        }
        else
        {
            // Network player. We receive data
            totalTime = (int)stream.ReceiveNext();
        }
    }
}
