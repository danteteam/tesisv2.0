﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DBConnection : MonoBehaviour
{
    private string platform;

    private string hora;

    private string reactionTime;

    [SerializeField]
    private string BASE_URL = "https://docs.google.com/forms/d/e/1FAIpQLSektnSn-sZU3nkoNqxMHfrUbr68brnkNuExWDAzHpuQJillVw/formResponse";

    public void sendInfo(string pPlatform, string pHora, string pReactionTime)
    {
        //Debug.Log("jijijiji miremo");
        platform = pPlatform;
        hora = pHora;
        reactionTime = pReactionTime;
        StartCoroutine(postInfo());
    }

    IEnumerator postInfo()
    {
        WWWForm form = new WWWForm();
        form.AddField("entry.1635538916", platform);
        form.AddField("entry.1963189168", hora);
        form.AddField("entry.906844661", reactionTime);
        byte[] rawData = form.data;
        WWW urlPage = new WWW(BASE_URL, rawData);
        yield return urlPage;
    }
}
