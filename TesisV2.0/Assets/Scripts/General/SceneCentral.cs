﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneCentral : MonoBehaviour
{

	// Use this for initialization
	void Start ()
    {
		
	}
	
    public void loadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
    }

	// Update is called once per frame
	void Update () {
		
	}
}
