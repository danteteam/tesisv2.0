﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuIManager : MonoBehaviour
{
    [Tooltip("Buttons that will be disabled to show the loading text")]
    public GameObject initialCanvas;

    [Tooltip("The UI Label to inform the user that the connection is in progress")]
    public GameObject loadingText;

    [Tooltip("Button that starts a playing session")]
    public GameObject startButton;

    public GameObject waitingText;

    [Tooltip("Text that will display my current player name in the network")]
    public Text playerIdText;


    [Tooltip("Array of player status texts that will show the player the other players that are or aren't connected to the session")]
    public Text[] playersStatusTexts;


    // Use this for initialization
    void Start ()
    {
        loadingText.SetActive(false);
        initialCanvas.SetActive(true);
        startButton.SetActive(false);
    }

    public void ConnectUI()
    {
        loadingText.SetActive(true);
        initialCanvas.SetActive(false);
        startButton.SetActive(false);
    }

    public void JoinedRoomUI(bool isHost)
    {
        loadingText.SetActive(false);
        if(isHost == true)
        {
            startButton.SetActive(true);
            waitingText.SetActive(false);
        }
        else
        {
            startButton.SetActive(false);
            waitingText.SetActive(true);
        }
    }

    public void DisconnectUI()
    {
        loadingText.SetActive(false);
        initialCanvas.SetActive(true);
        startButton.SetActive(false);
        waitingText.SetActive(false);
    }

    public void setPlayerName(string pName)
    {
        if (playerIdText != null)
        {
            playerIdText.text = pName;
        }
    }

    public void setPlayersStatusTexts(int index, string pStatus)
    {
        if (playersStatusTexts[index] != null)
        {
            playersStatusTexts[index].text = pStatus;
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
}
