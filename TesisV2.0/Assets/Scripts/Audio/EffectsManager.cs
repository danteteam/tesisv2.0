﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectsManager : MonoBehaviour
{
    public AudioClip[] menuEffects;

    public AudioClip[] inGameEffects;

    public static EffectsManager Instance;

    void Awake()
    {
        Instance = this;
    }

}
