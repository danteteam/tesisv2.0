﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBallAudio : Photon.PunBehaviour, IPunObservable
{
    public static FireBallAudio Instance;

    private AudioSource audioSource;


	// Use this for initialization
	void Awake ()
    {
        Instance = this;
        audioSource = GetComponent<AudioSource>();  	
	}
	
    public void playEffect(int effect, bool loopable)
    {
        audioSource.clip = EffectsManager.Instance.inGameEffects[effect];
        audioSource.loop = loopable;
        audioSource.Play();
    }

    public void stopEffects()
    {
        audioSource.loop = false;
        audioSource.Stop();
        audioSource.clip = null;
    }

	// Update is called once per frame
	void Update () {
		
	}

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {

    }
}
