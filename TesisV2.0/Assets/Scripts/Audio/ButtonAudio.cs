﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonAudio : MonoBehaviour, IPointerEnterHandler, ISelectHandler
{
    private AudioSource audioSource;

    private Button thisButton;

    public void OnPointerEnter(PointerEventData eventData)
    {
        // When button highlighted
        audioSource.clip = EffectsManager.Instance.menuEffects[0];
        audioSource.Play();
    }

    public void OnSelect(BaseEventData eventData)
    {
        // When button pressed
        audioSource.clip = EffectsManager.Instance.menuEffects[1];
        audioSource.Play();
    }

    // Use this for initialization
    void Awake ()
    {
        audioSource = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update ()
    {

    }
}
