﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NetworkManager : Photon.PunBehaviour, IPunObservable
{
    //---------------------------------------
    // Public Variables
    //---------------------------------------

    // Target platform of the game. This can be PC, Hololens, Oculus, Mobile.
    public LvlManager.Platform platform;

    /// <summary>
    /// The PUN loglevel. 
    /// </summary>
    private PhotonLogLevel Loglevel = PhotonLogLevel.ErrorsOnly;


    /// <summary>
    /// The maximum number of players per room. When a room is full, it can't be joined by new players, and so new room will be created.
    /// </summary>   
    [Tooltip("The maximum number of players per room. When a room is full, it can't be joined by new players, and so new room will be created")]
    public byte MaxPlayersPerRoom = 4;

    [Tooltip("Array of Menu Items Manager Scripts. This is used to optimize the way the UI elements are handled according to every possible device")]
    public MenuIManager[] menuItemsManagers;

    // Array of active players inside the session
    public bool[] activePlayers;

    //---------------------------------------
    // Private Variables
    //---------------------------------------

    /// Game Version
    private string _gameVersion = "0.1";

    /// Keep track of the current process. Since connection is asynchronous and is based on several callbacks from Photon, 
    /// we need to keep track of this to properly adjust the behavior when we receive call back by Photon.
    /// Typically this is used for the OnConnectedToMaster() callback.
    private bool isConnecting;

    // These are the options that will have the room that we're creating for the players
    private RoomOptions roomOptions;

    private MenuIManager mainMenuIManager;


    //---------------------------------------
    // Methods
    //---------------------------------------

    /// MonoBehaviour method called on GameObject by Unity during early initialization phase.
    void Awake()
    {
        PlayerPrefs.SetInt("Platform", (int) platform);

        mainMenuIManager = menuItemsManagers[(int)platform];
        // #NotImportant
        // Force LogLevel
        PhotonNetwork.logLevel = Loglevel;

        // #Critical
        // we don't join the lobby. There is no need to join a lobby to get the list of rooms.
        PhotonNetwork.autoJoinLobby = false;


        // #Critical
        // this makes sure we can use PhotonNetwork.LoadLevel() on the master client and all clients in the same room sync their level automatically
        PhotonNetwork.automaticallySyncScene = true;
    }

    // Use this for initialization
    void Start()
    {
        roomOptions = new RoomOptions { IsVisible = true, MaxPlayers = MaxPlayersPerRoom };
        DontDestroyOnLoad(this);
    }

    /// Start the connection process. 
    /// - If already connected, we attempt joining a random room
    /// - if not yet connected, Connect this application instance to Photon Cloud Network
    public void Connect()
    {
        if (PhotonNetwork.connectionState != ConnectionState.Disconnected)
        {
            return;
        }
        // keep track of the will to join a room, because when we come back from the game we will get a callback that we are connected, so we need to know what to do then
        isConnecting = true;
        mainMenuIManager.ConnectUI();

        // we check if we are connected or not, we join if we are , else we initiate the connection to the server.
        if (PhotonNetwork.connected)
        {
            // #Critical we need at this point to attempt joining a Random Room. If it fails, we'll get notified in OnPhotonRandomJoinFailed() and we'll create one.
            //RoomOptions roomOptions = new RoomOptions { IsVisible = true, MaxPlayers = MaxPlayersPerRoom };
            PhotonNetwork.JoinOrCreateRoom("Doom Room", roomOptions, TypedLobby.Default);
        }
        else
        {
            // #Critical, we must first and foremost connect to Photon Online Server.
            PhotonNetwork.ConnectUsingSettings(_gameVersion);
        }
    }


    public void loadLevel()
    {
        if (PhotonNetwork.isMasterClient)
        {
            PhotonNetwork.LoadLevel("MainLvlOnline");
        }
        //Debug.Log("The Room name is: " + PhotonNetwork.room.Name);
        for (int i = 0; i < PhotonNetwork.playerList.Length; i++)
        {
            PhotonPlayer temp = PhotonNetwork.playerList[i];
            //Debug.Log(temp.NickName + " is in the room " + PhotonNetwork.room.Name);
        }
    }

    //---------------------------------------------
    // Photon
    //--------------------------------------------

    public override void OnConnectedToMaster()
    {
        Debug.Log("CONECTADO AL MAESTRO");
        // we don't want to do anything if we are not attempting to join a room. 
        // this case where isConnecting is false is typically when you lost or quit the game, when this level is loaded, OnConnectedToMaster will be called, in that case
        // we don't want to do anything.
        if (isConnecting)
        {
            PhotonNetwork.JoinOrCreateRoom("Doom Room", roomOptions, TypedLobby.Default);
        }
    }

    public override void OnPhotonRandomJoinFailed(object[] codeAndMsg)
    {
        // #Critical: we failed to join a random room, maybe none exists or they are all full. No worries, we create a new room.
        PhotonNetwork.CreateRoom("Doom Room", roomOptions, TypedLobby.Default);
    }

    // This method is called when you as a player connect to the game room
    public override void OnJoinedRoom()
    {
        // #Critical: We only load if we are the first player, else we rely on  PhotonNetwork.automaticallySyncScene to sync our instance scene.
        // This part sets the players name according to it's connecting position
        PhotonPlayer[] players = PhotonNetwork.playerList;
        for (int i = 0; i < players.Length; i++)
        {
            PhotonPlayer temp = players[i];
            if (temp.IsLocal)
            {
                if (temp.NickName == "")
                {
                    temp.NickName = "Player " + i;
                }
                photonView.RPC("ActivatePlayer", PhotonTargets.All, i, true);
                char[] player = temp.NickName.ToCharArray();
                mainMenuIManager.setPlayerName("Player: " + (Int32.Parse(player[player.Length - 1] + "")));
            }
        }
        bool isHost = PhotonNetwork.isMasterClient;
        mainMenuIManager.JoinedRoomUI(isHost);

       // Debug.Log("INFO IMPORTANTE : InstantiateOnNetwork is: " + PhotonNetwork.InstantiateInRoomOnly + " , inRoom: " + PhotonNetwork.inRoom);
    }

    public override void OnPhotonPlayerConnected(PhotonPlayer other)
    {
        //Debug.Log("OnPhotonPlayerConnected() " + other.NickName); // not seen if you're the player connecting
        refreshConnectedPlayers();
    }

    public override void OnPhotonPlayerDisconnected(PhotonPlayer other)
    {
        // Debug.Log("OnPhotonPlayerDisconnected() " + other.NickName); // seen when other disconnects
        refreshConnectedPlayers();
    }

    public void refreshConnectedPlayers()
    {
        PhotonPlayer[] players = PhotonNetwork.playerList;
        for (int i = 0; i < 4; i++)
        {
            PhotonPlayer temp = null;
            if (i < players.Length)
            {
                temp = players[i];
            }
            if (temp == null)
            {
                // If there is no player in i position it means that position hasn't been occupied by any player
                photonView.RPC("ActivatePlayer", PhotonTargets.All, i, false);
            }
            else
            {
                photonView.RPC("ActivatePlayer", PhotonTargets.All, i, true);
            }
        }
        mainMenuIManager.setPlayerName(PhotonNetwork.player.NickName);
    }

    public void OnDestroy()
    {
        PhotonNetwork.LeaveRoom();
    }

    
    public void disconnect()
    {
        //PhotonNetwork.Disconnect();
    }



    public override void OnDisconnectedFromPhoton()
    {
        mainMenuIManager.DisconnectUI();
        isConnecting = false;
        Debug.LogWarning("NetworkManager: OnDisconnectedFromPhoton() was called by PUN");
    }

    [PunRPC]
    public void ActivatePlayer(int playerPos, bool playerState)
    {
        activePlayers[playerPos] = playerState;
        mainMenuIManager.setPlayersStatusTexts(playerPos, activePlayers[playerPos] + "");
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {

    }
}
