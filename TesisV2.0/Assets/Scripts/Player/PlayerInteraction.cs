﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.UI;


public class PlayerInteraction : Photon.PunBehaviour, IPunObservable
{
    //------------------------------------------------
    // Public Variables
    //------------------------------------------------

    //  Hand in which the ball will end up
    public GameObject hand;

    // Variable that tells if the player is an AI
    public bool isAI;

    // Maximum Decision Time for the AI to take a decision 
    public int decisionTime;

    // ID of the actual player
    public int playerID;

    public Material[] skins;

    public SkinnedMeshRenderer mesh;

    // Canvas Text for the Tingo, Tingo, Tango Announcement
    public Text announcementText;

    // Canvas Text for the session time
    public Text timeText;

    // Animator of the Ball
    public Animator announcementAnimator;

    // The local player instance. Use this to know if the local player is represented in the Scene
    public static GameObject LocalPlayerInstance;

    public ParticleSystem deathEffect;

    //------------------------------------------------
    // Private Variables
    //------------------------------------------------

    // This is the player that has given you the ball and you cannot give it to him
    private int restrictedPlayer = -1;

    // Reference to the fireball that the player has in his hands if he does so
    private GameObject fireBall;

    // Initial Rotation in Y 
    private float initialRotY;

    private int totalTime;

    // Hash for the change of state of tango
    private int tangoHash = Animator.StringToHash("Tango");

    private float receiveTime;

    private float sendingTime;


    //------------------------------------------------
    // Methods
    //------------------------------------------------

    void Awake()
    {
        // #Important
        // used in GameManager.cs: we keep track of the localPlayer instance to prevent instantiation when levels are synchronized
        if (photonView.isMine && !isAI)
        {
            PlayerInteraction.LocalPlayerInstance = this.gameObject;
        }
        // #Critical
        // we flag as don't destroy on load so that instance survives level synchronization, thus giving a seamless experience when levels load.
        DontDestroyOnLoad(this.gameObject);
    }

    // Use this for initialization
    void Start()
    {
        initialRotY = transform.rotation.eulerAngles.y;
        if (!isAI)
        {
            if (PhotonNetwork.player.IsMasterClient)
            {
                photonView.RPC("setAIInfo", PhotonTargets.All, null);
                totalTime = UnityEngine.Random.Range(15, 40);
            }
        }
    }

    [PunRPC]
    public void setPlayerInteraction(int playerID)
    {
        setupInfo(playerID);
        LvlManager.Instance.playersInteractions[playerID] = this;
    }


    [PunRPC]
    public void setAIInfo()
    {
        GameObject[] aiPlayers = GameObject.FindGameObjectsWithTag("AIPlayer");
        int initialIndex = PhotonNetwork.playerList.Length;
        for (int i = 0; i < aiPlayers.Length; i++)
        {
            GameObject temp = aiPlayers[i];
            temp.GetComponent<PlayerInteraction>().setupInfo(i + initialIndex);
            LvlManager.Instance.playersInteractions[i + initialIndex] = temp.GetComponent<PlayerInteraction>();
        }
    }

    public int getRestrictedPlayer()
    {
        return restrictedPlayer;
    }

    public void setupInfo(int pID)
    {
        playerID = pID;
        mesh.material = skins[pID];
        gameObject.name = "Player " + (pID);
    }

    // Method that tells if the player that the player is looking is restricted because he has passed the ball to you or not
    public bool isRestrictedPlayer(int playerName)
    {
        if (playerName == restrictedPlayer)
        {
            return true;
        }
        return false;
    }

    // Method that initializes the game
    public void initialize(int totalTimeP)
    {
        if (!isAI)
        {
            Debug.Log("Reinicio");
            timeText.text = totalTime + "";
            announcementText.text = "Tingo";
            announcementAnimator.SetBool(tangoHash, false);
        }
        deathEffect.gameObject.SetActive(false);
        //deathEffect.Stop();
        fireBall = null;
        totalTime = totalTimeP;
        StartCoroutine(startCountDown());
        Time.timeScale = 1f;
    }

    // IEnumerator that begins the session timer
    IEnumerator startCountDown()
    {
        while (totalTime > 0)
        {
            totalTime -= 1;
            if (!isAI)
            {
                timeText.text = totalTime + "";
            }
            yield return new WaitForSeconds(1f);
        }
        if (!isAI)
        {
            timeText.text = totalTime + "";
            announcementText.text = "Tango";
            announcementAnimator.SetBool(tangoHash, true);
        }
        Time.timeScale = 0.8f;
        StopAllCoroutines();
    }

    [PunRPC]
    public void takeBall(int restrictedID)
    {
        GameObject DaFireBall = LvlManager.Instance.FireBall;
        PhotonView firePV = DaFireBall.GetComponent<PhotonView>();
        firePV.RequestOwnership();
        //Debug.Log("El jugador restringido para mí es " + restrictedID);
        restrictedPlayer = restrictedID;
        DaFireBall.transform.parent = hand.transform;
        DaFireBall.transform.localPosition = Vector3.zero;
        fireBall = DaFireBall;
        receiveTime = Time.time;
        //Debug.Log("RECEIVE Player Name: " + gameObject.name + ", Platform: " + LvlManager.Instance.getCurrentPlatform().ToString() + ", Received Ball at : " + Time.time);
        /*
        Analytics.CustomEvent("ReceiveBall", new Dictionary<string, object>
        {
            {"Player Name", gameObject.name},
            {"Platform", LvlManager.Instance.getCurrentPlatform().ToString() },
            {"Event Time", totalTime}

        });
        */
        if (isAI == true && PhotonNetwork.isMasterClient)
        {
            photonView.RPC("startThrowinBehaviour", PhotonTargets.All, null);
        }
    }

    // Method that sends the ball to the destPlayer
    [PunRPC]
    public void sendBall(int destPlayer)
    {
        if (fireBall != null)
        {
            fireBall = null;
            //Debug.Log("My player id is: " + playerID + " and after sending the ball my state is " + fireBall);
            PhotonView.Find(destPlayer).RPC("takeBall", PhotonTargets.All, playerID);
        }

        sendingTime = Time.time;

        float responseTime = sendingTime - receiveTime;
        if(isAI == false && photonView.isMine == true)
        {
            string currentTime = System.DateTime.Now.ToString("yyy/MM/dd HH:mm:ss");
            LvlManager.Instance.sendReport(currentTime, responseTime + "");
        }
       // Debug.Log("SEND Player Name: " + gameObject.name + ", Platform: " + LvlManager.Instance.getCurrentPlatform().ToString() + ", Response Time : " + responseTime + ", Event Time: " + Time.time);
        /*Analytics.CustomEvent("Send The Fire", new Dictionary<string, object>
            {
                {"Player Name", gameObject.name},
                {"Platform", LvlManager.Instance.getCurrentPlatform().ToString() },
                {"Response Time", responseTime },
                { "Event Time", totalTime}
            });
            */
    }

    public GameObject findPlayer(string nickName)
    {
        GameObject answer = null;
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        for (int i = 0; i < players.Length; i++)
        {
            GameObject temp = players[i];
            PhotonView tempPV = temp.GetComponent<PhotonView>();
            PhotonPlayer pPlayer = tempPV.owner;
            if (pPlayer.NickName == nickName)
            {
                answer = temp;
                break;
            }
        }
        return answer;
    }

    public bool hasFireBall()
    {
        if (hand.transform.childCount > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
        /*
            if (fireBall != null)
        {
            return true;
        }
        else
            return false;
            */
    }


    public void explodeBall()
    {
        if (isAI)
        {
            StopAllCoroutines();
        }
        if (hasFireBall() == true)
        {
            deathEffect.gameObject.SetActive(true);
            deathEffect.Stop();
            deathEffect.Play();
            fireBall = null;
        }
    }

    //------------------------------------------------
    // AI
    //------------------------------------------------

    // Method that selects automatically the next player to pass the balls
    public PlayerInteraction selectPlayer()
    {
        PlayerInteraction answer = null;
        PlayerInteraction[] players = LvlManager.Instance.playersInteractions;
        bool hasFound = false;
        int destPosition = -1;
        while (hasFound == false)
        {
            destPosition = UnityEngine.Random.Range(0, players.Length);
            if (destPosition != (playerID - 1) && destPosition != restrictedPlayer)
            {
                answer = players[destPosition];
                bool isRestricted = isRestrictedPlayer(answer.playerID);
                if (isRestricted == false)
                {
                    hasFound = true;
                }
            }
        }
        //Debug.Log("Selected player is Player :" + destPosition);
        return answer;
    }

    [PunRPC]
    public void startThrowinBehaviour()
    {
        if (PhotonNetwork.isMasterClient)
        {
            StartCoroutine(throwingBehaviour());
        }
    }

    // IEnumerator that starts the throwing behaviour of the AI Player
    IEnumerator throwingBehaviour()
    {
        if (LvlManager.Instance.isOnSession)
        {
            //Debug.Log("My id is: " + playerID);
            PlayerInteraction[] players = LvlManager.Instance.playersInteractions;
            int optionablePlayer = 0;
            int time = UnityEngine.Random.Range(5, decisionTime + 1);
            while (time > 0)
            {
                // This is where the AI player teases to select his next objective
                time -= 1;
                if (optionablePlayer != (playerID))
                {
                    // If the optionable player is different than me ill face him
                    PlayerInteraction temp = players[optionablePlayer];
                    transform.LookAt(temp.gameObject.transform);
                }
                optionablePlayer++;
                if (optionablePlayer == players.Length)
                {
                    optionablePlayer = 0;
                }
                yield return new WaitForSeconds(0.4f);
            }
            // Takes the decision to select the next player to receive the ball by you
            PlayerInteraction selectedPlayer = selectPlayer();
            // Rotates towards the selected player
            transform.LookAt(selectedPlayer.gameObject.transform);
            selectedPlayer.GetComponentInChildren<PhotonView>().RPC("takeBall", PhotonTargets.All, playerID);
            //selectedPlayer.receiveBall(playerID, fireBall);
            fireBall = null;
            yield return new WaitForSeconds(0.4f);
            // Leaves the player in its initial rotation
            Vector3 newRot = transform.rotation.eulerAngles;
            newRot.y = initialRotY;
            transform.rotation = Quaternion.Euler(newRot.x, newRot.y, newRot.z);
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        throw new NotImplementedException();
    }

}
