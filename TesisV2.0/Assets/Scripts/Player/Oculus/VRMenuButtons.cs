﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VRMenuButtons : MonoBehaviour
{
    [SerializeField]
    private VRInteractiveItem m_InteractiveItem;       // Reference to the VRInteractiveItem to determine when to fill the bar.

    [SerializeField]
    private VRInput m_VRInput;                         // Reference to the VRInput to detect button presses.

    public Image buttonImage;

    private bool isLookingTheButton;

    private void OnEnable()
    {
        m_VRInput.OnDown += HandleDown;
        m_VRInput.OnUp += HandleUp;

        m_InteractiveItem.OnOver += HandleOver;
        m_InteractiveItem.OnOut += HandleOut;
    }


    private void OnDisable()
    {
        m_VRInput.OnDown -= HandleDown;
        m_VRInput.OnUp -= HandleUp;

        m_InteractiveItem.OnOver -= HandleOver;
        m_InteractiveItem.OnOut -= HandleOut;
    }

    private void HandleDown()
    {
        // If the user uses fire input while looking at the button
        if (isLookingTheButton)
            buttonImage.color = Color.red;
        GetComponent<Button>().onClick.Invoke();
    }


    private void HandleUp()
    {
        isLookingTheButton = false;
        buttonImage.color = Color.white;
    }


    private void HandleOver()
    {
        // The user is now looking at the button.
        isLookingTheButton = true;
        buttonImage.color = Color.yellow;
    }


    private void HandleOut()
    {
        isLookingTheButton = false;
        buttonImage.color = Color.white;
    }

}
