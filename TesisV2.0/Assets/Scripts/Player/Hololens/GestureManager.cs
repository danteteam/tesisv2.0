﻿using System;
using UnityEngine;
#if UNITY_WINRT_10_0
using UnityEngine.VR.WSA.Input;

[RequireComponent(typeof(GazeManager))]
public partial class GestureManager : Singleton<GestureManager>
{

    private MeshRenderer cursor;

    public PlayerInteraction playerInteraction;

    public GameObject HUD;

    /// <summary>
    /// To select even when a hologram is not being gazed at,
    /// set the override focused object.
    /// If its null, then the gazed at object will be selected.
    /// </summary>
    public GameObject OverrideFocusedObject
    {
        get; set;
    }

    /// <summary>
    /// Gets the currently focused object, or null if none.
    /// </summary>
    public GameObject FocusedObject
    {
        get { return focusedObject; }
    }

    private GestureRecognizer gestureRecognizer;
    private GameObject focusedObject;

    void Start()
    {
        if (HUD != null)
        {
            if(PhotonNetwork.player.IsLocal)
            {
                HUD.SetActive(true);
            }
            else
            {
                HUD.SetActive(false);
            }
        }
        // Create a new GestureRecognizer. Sign up for tapped events.
        gestureRecognizer = new GestureRecognizer();
        gestureRecognizer.SetRecognizableGestures(GestureSettings.Tap);

        gestureRecognizer.TappedEvent += GestureRecognizer_TappedEvent;

        // Start looking for gestures.
        gestureRecognizer.StartCapturingGestures();

        cursor = GetComponent<MeshRenderer>();
    }

    private void GestureRecognizer_TappedEvent(InteractionSourceKind source, int tapCount, Ray headRay)
    {
        if (focusedObject != null)
        {
            if (focusedObject.tag == "ShareableObject")
            {
                if (LvlManager.Instance.isOnSession == false && PhotonNetwork.isMasterClient)
                {
                    LvlManager.Instance.initializeGame();
                }
                // focusedObject.SendMessage("OnScorePoint");
                //GameManager.instance.OnScorePoint();

                // Does something

                Destroy(focusedObject);
            }
            else if (focusedObject.tag == "Player" || focusedObject.tag == "AIPlayer")
            {
                PlayerInteraction objPLayer = focusedObject.GetComponent<PlayerInteraction>();
                bool isRestricted = playerInteraction.isRestrictedPlayer(objPLayer.playerID);
                if (isRestricted == false)
                {
                    // You can pass the ball
                    PhotonView pV = objPLayer.GetComponentInParent<PlayerInteraction>().gameObject.GetComponent<PhotonView>();
                    cursor.material.color = Color.blue;
                    playerInteraction.sendBall(pV.viewID);
                }
                else
                {
                    cursor.material.color = Color.magenta;
                }
            }
            //focusedObject.SendMessage("OnSelect");
        }
    }

    void LateUpdate()
    {
        GameObject oldFocusedObject = focusedObject;

        if (GazeManager.Instance.Hit &&
            OverrideFocusedObject == null &&
            GazeManager.Instance.HitInfo.collider != null)
        {
            // If gaze hits a hologram, set the focused object to that game object.
            // Also if the caller has not decided to override the focused object.
            focusedObject = GazeManager.Instance.HitInfo.collider.gameObject;
            if (focusedObject.tag == "ShareableObject")
            {
                cursor.material.color = Color.yellow;
            }
            else if (focusedObject.tag == "Player" || focusedObject.tag == "AIPlayer")
            {
                PlayerInteraction objPLayer = focusedObject.GetComponent<PlayerInteraction>();
                char[] objectiveName = focusedObject.name.ToCharArray();
                int realObjID = Int32.Parse(objectiveName[focusedObject.name.Length - 1] + "");
                bool isRestricted = playerInteraction.isRestrictedPlayer(objPLayer.playerID);
                Debug.Log("Is Restricted? :O The answer is : " + isRestricted);
                if (isRestricted == true)
                {
                    // You cannot pass the ball
                    cursor.material.color = Color.red;
                }
                else
                {
                    // You can pass the ball
                    cursor.material.color = Color.green;
                }
            }
        }
        else
        {
            // If our gaze doesn't hit a hologram, set the focused object to null or override focused object.
            focusedObject = OverrideFocusedObject;
            cursor.material.color = Color.white;
        }

        if (focusedObject != oldFocusedObject)
        {
            // If the currently focused object doesn't match the old focused object, cancel the current gesture.
            // Start looking for new gestures.  This is to prevent applying gestures from one hologram to another.
            gestureRecognizer.CancelGestures();
            gestureRecognizer.StartCapturingGestures();
        }
    }

    void OnDestroy()
    {
        gestureRecognizer.StopCapturingGestures();
        gestureRecognizer.TappedEvent -= GestureRecognizer_TappedEvent;
    }
}
#endif