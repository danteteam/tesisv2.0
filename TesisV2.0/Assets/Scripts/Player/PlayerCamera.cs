﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerCamera : Photon.PunBehaviour
{
    //---------------------------------
    // Public Variables
    //---------------------------------

    // Camera Movement Speed
    [SerializeField]
    private float cameraSensitivity = 10f;

    public int rayDistance;

    // Axis used to rotate the camera and the hands
    public GameObject bodyRotationAxis;

    public Image HairCross;

    public Sprite normalHairCross;

    public Sprite dangereousHairCross;

    public PlayerInteraction playerInteraction;

    //---------------------------------
    // Private Variables
    //---------------------------------

    // Mouse X. Rotates around Y axis
    private float yaw = 0.0f;

    // Mouse Y. Rotates around X axis
    private float pitch = 0.0f;

    // Use this for initialization
    void Start()
    {
        if (!photonView.isMine)
        {
            Camera.main.enabled = false;
            GetComponent<AudioListener>().enabled = false;
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (photonView.isMine == false && PhotonNetwork.connected == true)
        {
            return;
        }
        else
        {
            yaw += Input.GetAxis("Mouse X") * cameraSensitivity;
            pitch -= Input.GetAxis("Mouse Y") * cameraSensitivity;
            transform.eulerAngles = new Vector3(pitch, yaw, 0f);

            // RayCast Part
            Vector3 forward = transform.TransformDirection(Vector3.forward);
            Debug.DrawRay(transform.position, forward * rayDistance, Color.green);
            RaycastHit hit;
            if (Physics.Raycast(transform.position, forward, out hit, rayDistance))
            {
                GameObject hitObject = hit.transform.gameObject;
                //Debug.Log("Estás mirando a: " + hitObject.name);
                if (hitObject.tag == "ShareableObject")
                {
                    HairCross.color = Color.yellow;
                    HairCross.sprite = normalHairCross;
                }
                else if (hitObject.tag == "Player" || hitObject.tag == "AIPlayer")
                {
                    PlayerInteraction objPLayer = hitObject.gameObject.GetComponent<PlayerInteraction>();
                    bool isRestricted = playerInteraction.isRestrictedPlayer(objPLayer.playerID);
                    if (isRestricted == true)
                    {
                        // You cannot pass the ball
                        HairCross.color = Color.red;
                        HairCross.sprite = dangereousHairCross;
                    }
                    else
                    {
                        // You can pass the ball
                        HairCross.color = Color.green;
                        HairCross.sprite = normalHairCross;

                        if (Input.GetButtonDown("Jump"))
                        {
                            if(playerInteraction.hasFireBall() == true)
                            {
                                PhotonView pV = objPLayer.GetComponentInParent<PlayerInteraction>().gameObject.GetComponent<PhotonView>();
                                playerInteraction.sendBall(pV.viewID);
                            }
                        }
                    }
                }
                else
                {
                    HairCross.color = Color.white;
                    HairCross.sprite = normalHairCross;
                }
            }
        }


    }
}
