﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : Photon.MonoBehaviour
{
    public LvlManager.Platform platform;

    void Start()
    {
        if (photonView.isMine)
        {
            GetComponent<PlayerInteraction>().enabled = true;
            GetComponent<Animator>().enabled = true;
            GetComponent<PlayerInteraction>().enabled = true;
            GetComponentInChildren<Canvas>().enabled = true;
            GetComponentInChildren<Camera>().enabled = true;
            GetComponentInChildren<AudioListener>().enabled = true;
            switch (platform)
            {
                case LvlManager.Platform.Pc:
                    GetComponentInChildren<PlayerCamera>().enabled = true;
                    break;
                case LvlManager.Platform.Oculus:
                    GetComponentInChildren<VRPlayerCamera>().enabled = true;
                    break;
#if UNITY_WINRT_10_0
                case LvlManager.Platform.Hololens:
                    GetComponentInChildren<GazeManager>().enabled = true;
                    GetComponentInChildren<GestureManager>().enabled = true;
                    GetComponentInChildren<HoloCursor>().enabled = true;
                    GetComponentInChildren<Canvas>().enabled = true;
                    break;
#endif
            }

        }
    }
}
